#!/bin/bash
echo on    
echo ###########################################################
echo #Script para clonar os repositorios do projeto da rumo.   #
echo #                                                         #
echo ###########################################################
echo #Autor: Deovan Zanol                                      #
echo ###########################################################
echo ###########################################################
echo Clonando administracao.
cd ..
git clone git@bitbucket.org:rumo_logistica/administracao.git
cd ./administracao
git checkout QA-GFT
echo ###########################################################
echo Clonando administracao-frontend.
cd ..
git clone git@bitbucket.org:rumo_logistica/administracao-frontend.git
cd ./administracao-frontend
git checkout QA-GFT
echo ###########################################################
echo #Repositorios clonado com sucesso!                        #
echo ###########################################################
sleep 10s
