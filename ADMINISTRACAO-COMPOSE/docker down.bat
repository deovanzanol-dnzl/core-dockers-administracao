@echo on    
@echo ###########################################################
@echo #Script para parar os containers do projeto da rumo.      #
@echo ###########################################################
@echo #Autor: Deovan Zanol                                      #
@echo ###########################################################
@echo ###########################################################
@echo Parar containers.
docker-compose down
docker container prune --force
@echo ###########################################################
timeout 10