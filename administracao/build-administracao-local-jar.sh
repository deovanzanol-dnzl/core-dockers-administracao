#!/bin/bash
echo on    

echo '###########################################################'
echo '#Script para buildar os repositorios do projeto da rumo.  #'
echo '#                                                         #'
echo '#Builda local                                             #'
echo '###########################################################'
echo '#Autor: Deovan Zanol                                      #'
echo '###########################################################'
echo '###########################################################'
echo Buildando administracao.
cd ../../administracao
mvn clean package -DskipTests  -Plocal
cp ./target/app*.jar ../core-dockers-administracao/administracao/app.jar
cd ../core-dockers
echo '###########################################################'
echo '#Building docker administracao                             #'
echo '###########################################################'
cd './administracao'
docker build -t administracao .
echo '###########################################################'
echo '#Repositorios buildado com sucesso!                       #'
echo '###########################################################'

sleep 10s