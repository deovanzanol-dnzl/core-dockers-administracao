@echo on    
@echo '###########################################################''
@echo '#Script para atualizar os repositorios do projeto da rumo.#'
@echo '###########################################################'
@echo #Autor: Deovan Zanol                                      #
@echo '###########################################################'
@echo '###########################################################'
@echo '###########################################################'
@echo Atualizando administracao.
cd ..\administracao
git pull
@echo '###########################################################'
@echo Atualizando administracao-frontend.
cd ..\administracao-frontend 
@echo '###########################################################'
@echo '#Atualizado!                                              #'
@echo '###########################################################'
timeout 10