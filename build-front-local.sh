#!/bin/bash
echo on    


echo ###########################################################
echo #Script para buildar os repositorios do projeto da rumo.  #
echo #                                                         #
echo #Builda local                                             #
echo ###########################################################
echo #Autor: Deovan Zanol                                      #
echo ###########################################################
echo ###########################################################
echo Buildando administracao Frontend.
cd ../administracao-frontend
npm install
npm run build -- --configuration=qa-gft --base-href=//web --deploy-url=//
rm -r '../core-dockers/administracao-frontend/dist/'
cp -r ./dist  '../core-dockers/administracao-frontend/dist/'
cp nginx-custom.conf  '../core-dockers/administracao-frontend/'
cd ../core-dockers-administracao
echo ###########################################################
echo #Building docker!                                         #
echo ###########################################################
cd './administracao-frontend'
docker build -t administracao-frontend .
echo ###########################################################
echo #Build concluido!                                         #
echo ###########################################################

sleep 10s